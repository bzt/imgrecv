;*
;* bios/optionrom.asm
;* https://gitlab.com/bztsrc/imgrec
;*
;* Copyright (C) 2020 bzt (bztsrc@gitlab)
;*
;* Permission is hereby granted, free of charge, to any person
;* obtaining a copy of this software and associated documentation
;* files (the "Software"), to deal in the Software without
;* restriction, including without limitation the rights to use, copy,
;* modify, merge, publish, distribute, sublicense, and/or sell copies
;* of the Software, and to permit persons to whom the Software is
;* furnished to do so, subject to the following conditions:
;*
;* The above copyright notice and this permission notice shall be
;* included in all copies or substantial portions of the Software.
;*
;* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
;* HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
;* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
;* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;* DEALINGS IN THE SOFTWARE.
;*

OPTIONROM equ 1

            ORG         0600h-32
            USE16
loader:     db          55h,0AAh                ;ROM magic
            db          (loader_end-loader)/512 ;size in 512 blocks
.executor:  jmp         near realmode_start     ;entry point
.checksum:  dw          0                       ;checksum
.name:      db          "IMGRECV", 0
            dw          0
            dd          0, 0
.pnpptr:    dw          0
.flags:     dd          0
realmode_start:
            include "imgrecv.asm"
            db          (511-($-loader+511) mod 512) dup 0
loader_end:

chksum = 0
repeat $-loader
    load b byte from (loader+%-1)
    chksum = (chksum + b) mod 100h
end repeat
store byte (100h-chksum) at (loader.checksum)
