Image Recevier
==============

This is a very minimal chain loader for different architectures. It loads an image through serial line, and then passes control.
It was inspired by mrvn's [raspbootin](https://github.com/mrvn/raspbootin), which I later [ported](https://github.com/bztsrc/raspi3-tutorial/tree/master/14_raspbootin64)
to AArch64. This loader supports chain loader mode (that is, the image is loaded to the same address as if it were loaded by
the original firmware). Unlike mrvn's version this loader is able to load not only raw images, but ELF binaries as well, which
makes development easier.

The ELF files will be loaded as-is, just like the raw images. Their program header *won't be* parsed, except for the entry point.
To get it work, you should use a linker script that includes the file headers and program headers in the text segment, otherwise
it should be the same as for the raw image's script.

For sending the images, you can use mrvn's original [raspbootcom](https://github.com/mrvn/raspbootin/tree/master/raspbootcom)
command line tool, but if you have a non-Linux PC or if you just prefer windowed applications with a GUI, then I would recommend
[USBImager](https://gitlab.com/bztsrc/usbimager) (available for Windows, MacOSX and Linux) with the `-S` flag instead.

You can find the chain loader implementations in their respective directories. All licensed under MIT.

Cheers,

bzt
